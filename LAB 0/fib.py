
def fib(idx):
# this is the fibonacci calculation
    a = 0
    b = 1
    # defines variables a and b for the fibonacci
    # calculation in last if statement
    if idx < 0:
        return "not calculable, please enter a positive integer."
    # returns promt for indeces that can not be calculated
    if idx == 0:
        return 0
    # returns fibonacci number for special index 0
    if idx >= 1:
        for idx in range(1, idx):
            c = a + b
            a = b
            b = c
        return b
    # returns fibonacci number for all positive integeres greater than
    # or equal to 1 by storing numbers as it calculates
        
if __name__ == '__main__':
# this is here so that the user interface only runs when the program
# is called to run, but not when it is imported
    def userint():
        try:
            idx = int(input('Please enter index: '))
        except ValueError:
            idx = -1
        # these four lines were testing to see if the index entered 
        # was an integer, and defines idx = -1 so that the idx when
        # plugged in falls under the first if statement because it is
        # uncalculable
        print('The fibonacci number is', fib(idx))
        # this displays the final answer
        end = input('enter q to quit or enter t to try again: ')
        if end == 'q':
            print ('Goodbye')
        if end == 't':
           userint()
           # these four lines are the end promt, to 
           # either continue or quit the program
    userint()
    # runs the user interface program